﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Guybrush
{
    /// <summary>
    /// La class Outils permet de donner d'avantage d'information sur une carte clair et ces parcelles
    /// </summary>
    class Outils
    {
        #region Variable
        /// <summary>
        /// Tableau à une dimension qui recupere les lignes des fichier carte
        /// </summary>
        private List<string> Parc;
        String str;
        /// <summary>
        /// Tableau à deux diemsions qui recupere les lettre sous forme de matrice
        /// </summary>
        private string[,] Carte;
        /// <summary>
        /// Tableau a une dimensions qui récupère toute les lettre de l'alphabet en minuscule
        /// </summary>
        private List<string> lettre;
        /// <summary>
        /// les variable l et k serve juste a compter, 
        /// m aide les procedure public à demander à la procedure priver d'afficher ce qu'il faut
        /// et Taille recupere la taille à comparer avec les parcelle
        /// </summary>
        private int l, m, k, Taille;
        /// <summary>
        /// sert a calculer la taille moyenne de l'aire des parcelle d'une île
        /// ou a affciher si les taille des parcelle sont inferieur à la variable Taille
        /// </summary>
        private double compt;
        

        #endregion
        #region Constructeur
        /// <summary>
        /// récupere les ligne des fichier carte
        /// </summary>
        /// <param name="Fichier">Indique l'emplacement des fichier pour acceder au carte clair</param>
        public Outils(string Fichier)
        {
            lettre = new List<string> { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            Parc = new List<string>();
            try
            {
                StreamReader sr = new StreamReader(Fichier);

                while ((str = sr.ReadLine()) != null)
                {
                    Parc.Add(str);
                }
                sr.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }
            //tableau deux dimensions pour contenir la carte clair
            Carte = new string[10, 10];
            for (int i = 0; i < 10; i++)
            {
                //Sépare les lettres de la ligne
                string ligne = Parc[i];
                for (int j = 0; j < 10; j++)
                {
                    //insere les lettre des parcelle dans une matrice
                    Carte[i, j] = Convert.ToString(ligne[j]);
                }
            }
        }
        #endregion

        #region Methode
        /// <summary>
        /// procedure privé qui lie la carte et affiche ce que demande les procedure public
        /// </summary>
        /// <param name="letter">permet de selectionner la parcelle a afficher</param>
        private void Parcelle(string letter)
        {
            k = 0;
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (letter == Carte[i, j])
                    {
                        if (m == 1)
                        {
                            //affiche les ccordonner dans la console
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.Write("({0},{1}) ", i, j);
                        }
                        k++;
                    }
                }
            }

            if (m == 0 && k > 0)
            {
                //affiche les parcelle avec leur nombre d'unites
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("PARCELLE {0} - {1} unites", letter, k);
            }
            if (m == 2 && k > 0)
            {
                //affiche la taille d'une parcelle spécifique
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                Console.WriteLine("Taille de la parcelle {0} : {1} unites", letter, k);
            }
            if (k == 0 && m == 2)
            {
                //affiche la taille d'une parcelle spécifique qui nexiste pas
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Parcelle {0} : inexistante", letter);
                Console.WriteLine("Taille de la parcelle {0} : {1} unites", letter, k);
            }
            if ((m == 3 || m == 4) && k >= Taille)
            {
                //affiche les parcelle avec leur unites supérieur à une taille spécifique
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("parcelle {0} : {1} unites", letter, k);
                m = 4;
            }
            if ((m == 3 && k < Taille) || (m == 5 && k > 0))
            {
                compt++;
            }
        }
        /// <summary>
        /// procedure public qui appelle la procedure priver pour afficher toute les parcelle existante 
        /// avec leur unites et leurs coordonner
        /// </summary>
        public void Tparcelle()
        {
            //prend un tableau et appelle la methode Parcelle pour afficher toute les parcelle avec leur coordonner
            for (l = 0; l < 26; l++)
            {
                for (m = 0; m < 2; m++)
                {
                    Parcelle(lettre[l]);
                }
            }
        }
        /// <summary>
        /// procedure public qui appelle la procedure priver pour afficher une parcelle choisi par l'utilisateur
        /// </summary>
        public void Uparcelle()
        {
            //demande a l'utiisateur une lettre et appelle la methode Parcelle pour afficher la parcelle demander
            m = 2;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Choissiser une parcelle");
            Console.WriteLine();
            Parcelle(Console.ReadLine());

        }
        /// <summary>
        /// procedure public qui appelle la procedure priver pour afficher les parcelle 
        /// avec un nombre d'unites supérieur ou égale a la taille demander a  l'utilisateur
        /// </summary>
        public void Tailleparcelle()
        {
            //demande une taille a l'utilisateur et appelle la methode Parcelle pour afficher les parcelle superieur ou egale à celle-ci
            m = 3;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine("Choissiser une taille");
            Console.WriteLine();
            Taille = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Parcelles de taille supérieure à {0}", Taille);
            for (l = 0; l < 26; l++)
            {
                Parcelle(lettre[l]);
            }
            if (compt == 26)
            {
                Console.WriteLine("Aucune parcelle");
            }
        }
        /// <summary>
        /// procedure public qui appelle la procedure priver pour afficher la taille moyenne des parcelle d'une île
        /// </summary>
        public void Aireparcelle()
        {
            Console.WriteLine();
            //calcul la taille moyenne des parcelle
            m = 5;
            double Aire = 0;
            double moyaire = 0;
            for (l = 0; l < 26; l++)
            {
                Parcelle(lettre[l]);
                Aire = Aire + k;
            }
            moyaire = Aire / compt;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Aire moyenne : {0:0.00}", moyaire);
        }
        #endregion
    }
}