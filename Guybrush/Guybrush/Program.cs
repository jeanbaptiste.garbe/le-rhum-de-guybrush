using System;

namespace Guybrush
{
    class Program
    {
        static void Main(string[] args)
        {
            Codage M = new Codage(@"C:\Users\cedri\le-rhum-de-guybrush\Guybrush\Scabb.Clair.txt");
            M.coder();
            M.Affichage();

            Codage M2 = new Codage(@"C:\Users\cedri\le-rhum-de-guybrush\Guybrush\Phatt.Clair.txt");
            M2.coder();
            M2.Affichage();
            
            Decodage D = new Decodage(@"C:\Users\cedri\le-rhum-de-guybrush\Guybrush\Scabb.Chiffre.txt");
            D.Decoder();
            D.AffichageCarte();

            Decodage D2 = new Decodage(@"C:\Users\cedri\le-rhum-de-guybrush\Guybrush\Phatt.Chiffre.txt");
            D2.Decoder();
            D2.AffichageCarte();   

            Outils O = new Outils(@"C:\Users\cedri\le-rhum-de-guybrush\Guybrush\Scabb.Clair.txt");
            O.Tparcelle();
            O.Uparcelle();
            O.Tailleparcelle();
            O.Aireparcelle();

            Outils O2 = new Outils(@"C:\Users\cedri\le-rhum-de-guybrush\Guybrush\Phatt.Clair.txt");
            O2.Tparcelle();
            O2.Uparcelle();
            O2.Tailleparcelle();
            O2.Aireparcelle();
        }
    }
}
