using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Guybrush
{
    /// <summary>
    /// Classe Décodage: Déchiffre la trame de Nombre et crée une Carte
    ///  à partir de ces nombre.
    /// </summary>
    class Decodage
    {
        #region Variables
        /// <summary>
        /// Tableau Unidimentionnel qui recupère la trame de nombres.
        /// </summary>
        private string[] ListeDeNombresADechiffrer;

        /// <summary>
        /// Tableau à 2 Dimensions qui place les nombres sous forme de carte 10x10.
        /// </summary>
        private int[,] CarteDeNombre = new int[10, 10];

        /// <summary>
        /// Tableau à 2 Dimensions qui contient la Carte obtenu.
        /// </summary>
        private string[,] Carte = new string[10, 10];

        /// <summary>
        /// Booleen qui vérifie la presence d'un frontiere au Nord de l'unité
        /// de la parcelle.
        /// </summary>
        private bool FrontiereAuNord = false;

        /// <summary>
        /// Booleen qui vérifie la presence d'un frontiere à l'Est de l'unité
        /// de la parcelle.
        /// </summary>
        private bool FrontiereAlEst = false;

        /// <summary>
        /// Booleen qui vérifie la presence d'un frontiere à l'Ouest de l'unité
        /// de la parcelle.
        /// </summary>
        private bool FrontiereAlOuest = false;

        /// <summary>
        /// Un second Booleen qui vérifie la presence d'un frontiere au Nord
        /// l'unité de la parcelle. Utiisé dans des cas spécifique.
        /// </summary>
        private bool FrontiereAuNord2 = false;

        /// <summary>
        /// Un second Booleen qui vérifie la presence d'un frontiere à l'Est
        /// de l'unité de la parcelle. Utiisé dans des cas spécifique.
        /// </summary>
        private bool FrontiereAlEst2 = false;
        #endregion


        #region Constructeur
        /// <summary>
        /// Constructeur qui lit la trame de Nombre à partir de la localisation
        /// du fichier contenant la carte chiffée 
        /// </summary>
        /// <param name="CheminAccesFichierPointChiffre">Chemin d'Acces du
        /// Fichier contenant la trame de nombre "Fichier.Chiffre.txt"</param>
        public Decodage(string CheminAccesFichierPointChiffre)
        {
            try
            {
                // Instructions pouvant échouer
                StreamReader SR = new StreamReader(CheminAccesFichierPointChiffre);

                ListeDeNombresADechiffrer = ExtractionDeSymboles(SR.ReadLine());

                TransformationStringEnInt();
                SR.Close();
            }
            catch (Exception e)
            {
                // Executé uniquement si une instruction dans le try échoue
                // e est alimenté par Windows avec un msg d'erreur
                Console.WriteLine(e);   //Affihce le Message d'Erreur
                return;                 // On Quite le Constructeur
            }
        }
        #endregion

        #region Methodes Publiques
        /// <summary>
        /// Procédure Public permettant d'Afficher la Carte obtenu apres son décodage
        /// </summary>
        public void AffichageCarte()
        {
            int i;
            int j;
            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 10; j++)
                {
                    if (Carte[i, j] == "M")
                    {
                        // Coloration de la Lettre en Bleu
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write(Carte[i, j]);
                    }

                    else if (Carte[i, j] == "F")
                    {
                        // Coloration de la Lettre en Vert
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write(Carte[i, j]);
                    }

                    else
                    {
                        // Coloration de la Lettre en Blanc
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write(Carte[i, j]);
                    }
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Procédure Publique qui a pour but de décoder la trame de nombre
        /// </summary>
        public void Decoder()
        {
            int i;
            int j;
            int l = 0;

            // Creation de Tableau à 1 Dimension contenant les lettres qui vont
            // être attribué aux unités
            string[] Lettres = {"a", "b", "c", "d", "e", "f", "g", "h", "i",
                "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u",
                "v", "w", "x", "y", "z", };

            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 10; j++)
                {
                    // Lorsque la valeur de la matrice CarteDeNombre aux coordonnées
                    // I,J est Supperieur à 64, la lettre "M" est attribué à la matrice
                    // Carte au coordonnées respectives.
                    if (CarteDeNombre[i, j] >= 64)
                    {
                        Carte[i, j] = "M";
                    }

                    // Lorsque la valeur de la matrice CarteDeNombre aux coordonnées
                    // I,J qui est entre 32 et 64, la lettre "F" est attribué à la matrice
                    // Carte au coordonnées respectives.
                    else if (CarteDeNombre[i, j] >= 32 && CarteDeNombre[i, j] < 64)
                    {
                        Carte[i, j] = "F";
                    }

                    // Attrubution des lettres dans la matrice Carte lorsque la
                    // valeur du matrice CarteDeNombre au coordonnées I,J est
                    // inferieur à 32
                    else if (CarteDeNombre[i, j] < 32)
                    {
                        // Premiere Valeur
                        if (i == 0 && j == 0)
                        {
                            Carte[i, j] = Lettres[l];
                            l++;
                        }

                        // Premrière Ligne
                        else if (i == 0 && j > 0)
                        {
                            // Vérification si l'unité précédent possède une
                            // frontière à l'Est
                            FrontiereEst(CarteDeNombre[i, j - 1]);

                            // Si oui, l'unité aux coordonnées I,J prend une
                            // nouvelle lettre
                            if (FrontiereAlEst == true)
                            {
                                Carte[i, j] = Lettres[l];
                                FrontiereAlEst = false;
                                l++;
                            }

                            // Sinon, elle prend la même lettre que l'unité précédent
                            else
                            {
                                Carte[i, j] = Carte[i, j - 1];
                            }
                        }

                        // Première Colone
                        else if (i > 0 && j == 0)
                        {
                            // Vérifie si l'unité aux coordonnées I,J possède
                            // une frontière au Nord mais également vérifié si
                            // l'unité juste au dessus de lui possède une
                            // frontière au Sud
                            FrontiereNord(CarteDeNombre[i, j]);

                            // Si les deux Booléen sont vrai, l'unité au coodonnées
                            // I,J prend une nouvelle lettre
                            if (FrontiereAuNord == true)
                            {
                                Carte[i, j] = Lettres[l];
                                FrontiereAuNord = false;
                                l++;
                            }

                            // Sinon elle prend la même lettre que l'unité juste au dessus
                            else
                            {
                                Carte[i, j] = Carte[i - 1, j];
                            }
                        }

                        // Le Reste
                        else
                        {
                            // Tant que la matrice n'a pas atteint sa derniere colone
                            // ça vérifie si l'unité à l'Est de l'unité au coordonnées
                            // I,J possède une frontière à l'Ouest
                            if (j < 9)
                            {
                                FrontiereOuest(CarteDeNombre[i, j + 1]);
                            }

                            // Vérifie si l'unité aux coordonnées I,J possède
                            // une frontière au Nord 
                            FrontiereNord(CarteDeNombre[i, j]);

                            // Vérifie si l'unité précédent possède une frontière
                            // à l'Est
                            FrontiereEst(CarteDeNombre[i, j - 1]);
                            
                            // Si les 4 booléen sont vrai
                            if (FrontiereAlEst == true && FrontiereAuNord == true && FrontiereAlOuest == true)
                            {
                                // Vérification si c'est un cas spécial
                                // Vérifie si l'unité aux coordonnées I,J possède
                                // une frontière à l'Est
                                FrontiereEst2(CarteDeNombre[i, j]);

                                // Si elle ne possède pas de frontière Est
                                // C'est donc un cas spétial
                                if (FrontiereAlEst2 == false)
                                {
                                    // Elle vérifie si l'unité suivant, possède
                                    // une frontière au Nord
                                    FrontiereNord2(CarteDeNombre[i, j + 1]);

                                    // Si ce n'est pas le cas. l'unité au coordonnées
                                    // I,J prend la même lettre que l'unité se touvant
                                    // au Nord-Est de lui
                                    if (FrontiereAuNord2 == false)
                                    {
                                        Carte[i, j] = Carte[i - 1, j + 1];
                                        FrontiereAuNord = false;
                                        FrontiereAlOuest = false;
                                        FrontiereAlEst = false;
                                    }

                                    // Sinon elle prend une nouvelle lettre
                                    else
                                    {
                                        Carte[i, j] = Lettres[l];
                                        FrontiereAuNord2 = false;
                                        FrontiereAuNord = false;
                                        FrontiereAlOuest = false;
                                        FrontiereAlEst = false;
                                        l++;
                                    }
                                }

                                // Si ce n'es pas un cas spécial, elle prend
                                // une nouvelle lettre
                                else
                                {
                                    Carte[i, j] = Lettres[l];
                                    FrontiereAuNord = false;
                                    FrontiereAlOuest = false;
                                    FrontiereAlEst = false;
                                    FrontiereAlEst2 = false;
                                    l++;
                                }
                            }

                            // Si l'unité précédent ne possède pas de frontère
                            // à l'Est, l'unité aux coordonnées I,J prend la
                            // même lettre que l'unité précédent
                            else if (FrontiereAlEst == false && FrontiereAuNord == true)
                            {
                                Carte[i, j] = Carte[i, j - 1];
                                FrontiereAuNord = false;
                            }

                            // Si l'unité aux coordonnées I,J ne possède pas de
                            // frontière au Nord, elle prends la même lettre
                            // que l'unité au dessus
                            else if (FrontiereAlEst == true && FrontiereAuNord == false)
                            {
                                Carte[i, j] = Carte[i - 1, j];
                                FrontiereAlEst = false;
                            }

                            // Si l'unité précédent ne possède pas de frontère
                            // à l'Est, et que l'unité aux coordonnées I,J ne
                            // possède pas de frontière au Nord, l'unité I,J
                            // prends la même lettre que l'unité au dessus
                            else if (FrontiereAlEst == false &&FrontiereAuNord == false)
                            {
                                Carte[i, j] = Carte[i - 1, j];
                            }

                            // Sinon, dans les autres cas, elle prend une
                            // nouvelle lettre
                            else
                            {
                                Carte[i, j] = Carte[i, j - 1];
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Methodes Privées
        /// <summary>
        /// Procédure Privé qui prends les nombres extraite depuis la trame
        /// et le rempli dans la matrice CarteDeNombre tout en convertissant
        /// les nombres en chaine de caractère en entier
        /// </summary>
        private void TransformationStringEnInt()
        {
            int i;
            int j;
            int k = 0;

            for (i = 0; i < 10; i++)
            {
                for (j = 0; j < 10; j++)
                {
                    CarteDeNombre[i, j] = Convert.ToInt32(ListeDeNombresADechiffrer[k]);
                    k++;
                }
            }
        }

        /// <summary>
        /// Procédur Privé qui extrait les symboles ":" et "|" depuis la trame
        /// </summary>
        /// <param name="LigneFichier">Ligne du Fichier.chiffre.txt: la trame
        /// de Nombres</param>
        /// <returns>Retourne un chiane de caratères composé uniquement de
        /// nombres</returns>
        private string[] ExtractionDeSymboles(string LigneFichier)
        {
            string[] Tab = LigneFichier.Split(':', '|');

            return Tab;
        }

        /// <summary>
        /// Procédure qui vérifie la presensce d'une frontière Nord à partir du nombre de l'unité chiffré
        /// </summary>
        /// <param name="Chiffre">Le Nombre de la Matrice CarteDeNombre qui a besoin d'une vérification</param>
        private void FrontiereNord(int Chiffre)
        {
            if (Chiffre % 2 == 1)
            {
                // Frontiere Nord
                FrontiereAuNord = true;
            }
        }

        /// <summary>
        /// Procédure qui vérifie la presensce d'une frontière Est à partir du nombre de l'unité chiffré
        /// </summary>
        /// <param name="Chiffre">Le Nombre de la Matrice CarteDeNombre qui a besoin d'une vérification</param>
        private void FrontiereEst(int Chiffre)
        {
            if (Chiffre - (2 * 2 * 2) >= 0)
            {
                // Frontiere Est
                FrontiereAlEst = true;
            }
        }

        /// <summary>
        /// Procédure qui vérifie la presensce d'une frontière Ouest à partir du nombre de l'unité chiffré
        /// </summary>
        /// <param name="Chiffre">Le Nombre de la Matrice CarteDeNombre qui a besoin d'une vérification</param>
        private void FrontiereOuest(int Chiffre)
        {
            if (Chiffre - 2 >= 0)
            {
                // Frontière Ouest
                FrontiereAlOuest = true;
            }
        }

        /// <summary>
        /// Procédure qui vérifie la presensce d'une frontière Nord à partir du nombre de l'unité chiffré. Utilisé lors des cas spéciaux.
        /// </summary>
        /// <param name="Chiffre">Le Nombre de la Matrice CarteDeNombre qui a besoin d'une vérification</param>
        private void FrontiereNord2(int Chiffre)
        {
            if (Chiffre % 2 == 1)
            {
                // Frontiere Nord
                FrontiereAuNord2 = true;
            }
        }

        /// <summary>
        /// Procédure qui vérifie la presensce d'une frontière Est à partir du nombre de l'unité chiffré. Utilisé lors des cas spéciaux.
        /// </summary>
        /// <param name="Chiffre">Le Nombre de la Matrice CarteDeNombre qui a besoin d'une vérification</param>
        private void FrontiereEst2(int Chiffre)
        {
            if (Chiffre - (2 * 2 * 2) >= 0)
            {
                // Frontiere Nord
                FrontiereAlEst2 = true;
            }
        }
        #endregion
    }
}
