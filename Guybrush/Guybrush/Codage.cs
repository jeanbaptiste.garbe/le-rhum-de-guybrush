using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Guybrush
{
    /// <summary>
    /// Classe codage permet de convertir une carte claire en une trame coder
    /// </summary>
    public class Codage
    {
        #region Variables

        /// <summary>
        /// Contient les lettres qui constituent la carte ligne par ligne
        /// </summary>
        private List<string> Literalmap;
        /// <summary>
        /// Contient les lettres qui constituent la carte séparemment
        /// </summary>
        private string[,] map;
        /// <summary>
        /// Regarde s'il n'y a pas d'erreur pour afficher la map
        /// </summary>
        private bool afficheOK = true;
        /// <summary>
        /// Contient la carte coder
        /// </summary>
        private string trame = "";
        #endregion

        #region Constructeur
        /// <summary>
        /// Récupére la carte dans un fichier et la décomposer 
        /// </summary>
        /// <param name="CheminAccesFichier">Indication du chemin pour accéder au fichier de la carte claire</param>
        public Codage(string CheminAccesFichier)
        {
            Literalmap = new List<string>();
            try
            {
                StreamReader SR = new StreamReader(CheminAccesFichier);
                string str; 

                while ((str = SR.ReadLine()) != null)
                {
                    Literalmap.Add(str);
                }
                SR.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                afficheOK = false;
                return;
            }

            // Sépare tout les caractères et les met dans une matrice
            map = new string[10, 10];

            for (int i = 0; i < 10; i++)
            {
                string ligne = Literalmap[i];   //Sépare la ligne en plusieurs lettres
                for (int j=0; j<10; j++)
                {
                    map[i, j] = Convert.ToString(ligne[j]);
                }
            }
        }
        #endregion

        #region Méthodes

        /// <summary>
        /// Transforme la carte claire en une trame et la stocke dans une variable string
        /// 2^0 : 1 : frontière nord
        /// 2^1 : 2 : frontière ouest
        /// 2^2 : 4 : frontière sud
        /// 2^3 : 8 : frontière est
        /// 64 : Mer
        /// 32 : Forêt
        /// </summary>
        public void coder()
        {
            int compt = 0, nord = 1, ouest = 2, sud = 4, est = 8, mer = 64, foret = 32;
            for (int i=0; i<10; i++)
            {
                for(int j=0; j<10; j++)
                {
                    compt = 0;
                    switch (map[i, j])
                    {
                        case "M":
                            compt += mer;
                            break;
                        case "F": 
                            compt += foret;
                            break;
                        default:
                            compt += 0;
                            break;
                    }

                    // coin en haut à gauche
                    if (i == 0 && j == 0)
                    {
                        compt += nord + ouest;
                        compt += droit(i, j);
                        compt += bas(i, j);
                    }
                    //1er ligne sans les coins
                    else if (i == 0 && j != 0 && j != 9)
                    {
                        compt += nord;
                        compt += bas(i, j);
                        compt += gauche(i, j);
                        compt += droit(i, j);
                    }
                    // coin en haut à droite
                    else if(i==0 && j == 9)
                    {
                        compt += nord + est;
                        compt += gauche(i, j);
                        compt += bas(i, j);
                    }

                    // 1er colonne sans les coins
                    else if(i != 0 && i != 9 && j == 0)
                    {
                        compt += ouest;
                        compt += haut(i,j);
                        compt += droit(i,j);
                        compt += bas(i,j);
                    }
                    // dernière colonne sans les coins
                    else if (i != 0 && i != 9 && j == 9)
                    {
                        compt += est;
                        compt += haut(i, j);
                        compt += gauche(i, j);
                        compt += bas(i, j);
                    }
                    // coin en bas à gauche
                    else if (i == 9 && j == 0)
                    {
                        compt += ouest + sud;
                        compt += droit(i, j);
                        compt += haut(i, j);
                    }
                    //dernière ligne sans les coins
                    else if(i == 9 && j != 0 && j != 9)
                    {
                        compt += sud;
                        compt += haut(i, j);
                        compt += gauche(i, j);
                        compt += droit(i, j);
                    }
                    //coin en bas à droite
                    else if(i==9 && j == 9)
                    {
                        compt += sud + est;
                        compt += haut(i, j);
                        compt += gauche(i, j);
                    }
                    //gère l'intérieur excepté les extrémitées
                    else
                    {
                        compt += haut(i, j);
                        compt += gauche(i, j);
                        compt += bas(i, j);
                        compt += droit(i, j);
                    }

                    //Si on n'est pas en fin de ligne on met ":" sinon on change de ligne en l'indiquant via "|"
                    if (j != 9)
                    {
                        trame += Convert.ToString(compt) + ":";
                    }
                    else
                    {
                        trame += Convert.ToString(compt) + "|";
                    }
                }
            }
        }

        
        /// <summary>
        /// Affiche la carte en caractère séparé et coder
        /// </summary>
        public void Affichage()
        {
            if (afficheOK)
            {
                Console.WriteLine(trame + "\n\n");
            }
        }
        #endregion

        #region Fonctions
        /// <summary>
        /// Vérifie si la case au nord est la identique et donc s'il y a une frontière ou non
        /// </summary>
        /// <param name="i">numéro de la ligne de la matrice map</param>
        /// <param name="j">numéro de la colonne de la matrice map</param>
        /// <returns>Retourne 1 car la frontière haut (nord) correspond à 1</returns>
        private int haut(int i, int j)
        {
            if (map[i, j] == map[i - 1, j])
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// Vérifie si la case à l'ouest est la identique et donc s'il y a une frontière ou non
        /// </summary>
        /// <param name="i">numéro de la ligne de la matrice map</param>
        /// <param name="j">numéro de la colonne de la matrice map</param>
        /// <returns>Retourne 2 car la frontière gauche (ouest) correspond à 2</returns>
        private int gauche(int i, int j)
        {
            if (map[i, j] == map[i, j - 1])
            {
                return 0;
            }
            else
            {
                return 2;
            }
        }

        /// <summary>
        /// Vérifie si la case au sud est identique et donc s'il y a une frontière ou non
        /// </summary>
        /// <param name="i">numéro de la ligne de la matrice map</param>
        /// <param name="j">numéro de la colonne de la matrice map</param>
        /// <returns>Retourne 4 car la frontière bas (sud) correspond à 4</returns>
        private int bas(int i, int j)
        {
            if (map[i, j] == map[i + 1, j])
            {
                return 0;
            }
            else
            {
                return 4;
            }
        }

        /// <summary>
        /// Vérifie si la case à l'est est identique et donc s'il y a une frontière ou non
        /// </summary>
        /// <param name="i">numéro de la ligne de la matrice map</param>
        /// <param name="j">numéro de la colonne de la matrice map</param>
        /// <returns>Retourne 8 car la frontière droit (est) correspond à 8</returns>
        private int droit(int i, int j)
        {
            if (map[i, j] == map[i, j + 1])
            {
                return 0;
            }
            else
            {
                return 8;
            }
        }
        #endregion
    }
}
